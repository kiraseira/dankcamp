# dankcamp
Dank bandcamp parser for node.js, that grabs some data including a link to a streamable mp3 from a bandcamp song page, 
then you can feed the link to mpv/vlc/winamp/whatever and listen to muzak. Or just grab other data to provide info about a song.

Please use the module responsibly and remember to **support artists you like by buying their music**.

# How to use Pepega
The package provides 1 function: `parseBandcamp` which asks for two parameters:
* `in_url` required: full URL to a song page on bandcamp (string)
* `returnRaw` optional: do you want the full info JSON (boolean). Default is no, you only get a simplified JSON with just artist, title, mp3 link and timestamp.

The function returns a Promise. Resolves with the mentioned JSONs or rejects on various errors.

Example simplified returned JSON:

```json
{ artist: "Big Boomer Ben",
  title: "Down With The Script Tags",
  mp3Path: "http://somewhere.com/whatever.mp3",
  datePublished: "29 March 2020 06:30:20 GMT"
 }
 ```
 
Full JSON is kinda big, I suggest you run the command in debugger one with that parameter and study its fields if youo need that. Easy to understand btw, its just big.

# License
dankcamp is free software, you are welcome to use, copy, modify and redistribute it under 
the term found in the `LICENSE` file.

This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

# Hol' up ain't this shit illegal?
Please read [Bandcamp's stance about this issue](https://get.bandcamp.help/hc/en-us/articles/360007902173-I-heard-you-can-steal-music-on-Bandcamp-What-are-you-doing-about-this-)

