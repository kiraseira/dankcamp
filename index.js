const got = require("got");
const htmlparser = require("node-html-parser");

function parseBandcamp(in_url, returnRaw=false){
return new Promise(async (resolve, reject) => {
	if(in_url.length<28){
		reject("invalid URL");
		return;
	}
	//we use a popular Chrome user-agent just in case
	const https_options = {
		url: in_url,
		method: 'GET',
		headers: {
			'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'
		},
		timeout: 2000,
		retry: 0 };
	let webdata;
	try{
		webdata = await got(https_options);
	}
	catch(err){
		reject(err);
		return;
	}
	if(webdata.statusCode != 200){
		reject(`http ${webdata.statusCode}`);
		return;
	}
	if(webdata.body.lendth === 0){
		reject(`empty reply`);
		return;
	}
	let h;
	try{
		h = JSON.parse(htmlparser.parse(webdata.body).querySelector("script").childNodes[0].rawText);
	}
	catch(err){
		reject(err);
		return;
	}
	if(returnRaw){
		resolve(h);
		return;
	} else {
		let retval = {	artist: h.byArtist.name,
						title: h.name,
						duration: h.additionalProperty.find(p => p.name==="duration_secs").value,
						mp3Path: h.additionalProperty.find(p => p.name==="file_mp3-128").value,
						datePublished: h.datePublished };
		resolve(retval);
	}				
					
					
	
});
}

exports.parseBandcamp = parseBandcamp;
